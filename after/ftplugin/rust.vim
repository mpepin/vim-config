set expandtab copyindent preserveindent
set softtabstop=0 shiftwidth=2 tabstop=2

nnoremap <Leader>r :!cargo run<Enter>
nnoremap <Leader>t :!cargo test<Enter>
nnoremap <Leader>R :!cargo run --release<Enter>
nnoremap <Leader>m :!cargo build<Enter>
nnoremap <Leader>b :!cargo bench<Enter>
nnoremap <Leader>c :!cargo clean<Enter>

syntax region Statement matchgroup=Delimiter start="\$" end="\$"
syntax region Statement matchgroup=Delimiter start="\$\$" end="\$\$""
syntax region Statement matchgroup=Delimiter start="\\begin{.*}" end="\\end{.*}" contains=Statement
syntax region Statement matchgroup=Delimiter start="{" end="}" contains=Statement

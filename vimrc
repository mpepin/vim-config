set nocompatible
let mapleader = ","

" --- Syntax & filetypes ------------------------------------------------------

syn on
filetype plugin indent on

autocmd BufRead,BufNewFile,BufFilePre *.md setfiletype markdown
autocmd BufRead,BufNewFile,BufFilePre *.lus setfiletype lustre
autocmd BufRead,BufNewFile,BufFilePre *.mj setfiletype minijazz
autocmd BufRead,BufNewFile,BufFilePre *.net setfiletype netlist
autocmd BufRead,BufNewFile,BufFilePre *.cl setfiletype opencl
autocmd BufRead,BufNewFile,BufFilePre *.pl setfiletype prolog " perl -> oups
autocmd BufRead,BufNewFile,BufFilePre *.rml setfiletype ocaml
autocmd BufRead,BufNewFile,BufFilePre *.cbl setfiletype cbl
autocmd BufRead,BufNewFile /etc/nginx/*,/usr/local/nginx/conf/* if &ft == '' | setfiletype nginx | endif

let g:powerline_pycmd="py3"
set laststatus=2

let g:opamshare = substitute(system('opam config var share'),'\n$','','''')
execute "set rtp+=" . g:opamshare . "/merlin/vim"

" --- Tunning général ---------------------------------------------------------

set list
set listchars=trail:·,tab:\ \ ,nbsp:˽

set backspace=indent,eol,start

" Encodage
set encoding=utf-8
set fileencoding=utf-8

" Tabs
set tabstop=4
set shiftwidth=4
set smartindent
set expandtab

" Look
set background=dark
set number
set ruler
set lazyredraw

" Recherche
set ignorecase
set hlsearch
set incsearch

" taille des lignes
set textwidth=80

" espaces insécables
highlight NBSPError ctermbg=red guibg=red
match NBSPError " "


" --- Caca généré automatiquement ------------------------------------------

" ## added by OPAM user-setup for vim / base ## 93ee63e278bdfc07d1139a748ed3fff2 ## you can edit, but keep this line
let s:opam_share_dir = system("opam config var share")
let s:opam_share_dir = substitute(s:opam_share_dir, '[\r\n]*$', '', '')

let s:opam_configuration = {}

function! OpamConfOcpIndent()
  execute "set rtp^=" . s:opam_share_dir . "/ocp-indent/vim"
endfunction
let s:opam_configuration['ocp-indent'] = function('OpamConfOcpIndent')

function! OpamConfOcpIndex()
  execute "set rtp+=" . s:opam_share_dir . "/ocp-index/vim"
endfunction
let s:opam_configuration['ocp-index'] = function('OpamConfOcpIndex')

function! OpamConfMerlin()
  let l:dir = s:opam_share_dir . "/merlin/vim"
  execute "set rtp+=" . l:dir
endfunction
let s:opam_configuration['merlin'] = function('OpamConfMerlin')

let s:opam_packages = ["ocp-indent", "ocp-index", "merlin"]
let s:opam_check_cmdline = ["opam list --installed --short --safe --color=never"] + s:opam_packages
let s:opam_available_tools = split(system(join(s:opam_check_cmdline)))
for tool in s:opam_packages
  " Respect package order (merlin should be after ocp-index)
  if count(s:opam_available_tools, tool) > 0
    call s:opam_configuration[tool]()
  endif
endfor
" ## end of OPAM user-setup addition for vim / base ## keep this line
" ## added by OPAM user-setup for vim / ocp-indent ## d83c0bda5709e0a4853aebb309cec341 ## you can edit, but keep this line
if count(s:opam_available_tools,"ocp-indent") == 0
  source "/home/martin/.opam/default/share/ocp-indent/vim/indent/ocaml.vim"
endif
" ## end of OPAM user-setup addition for vim / ocp-indent ## keep this line

" Vim syntax file
" Language:       nanoCBL
" Filenames:      *.cbl

" nanoCBL is case sensitive
syntax case match

" Mismatches
syntax match Error ")"
syntax match Error "\*)"

" Builtin operators and symbols
syntax match Statement "+"
syntax match Statement "||"
syntax match Statement "\."
syntax match Statement "new"
syntax match Statement "0"
syntax match Statement "ν"

" Constants
syntax keyword Constant false true
syntax match Constant "\[\]"

" Brackets and co
syntax region None matchgroup=Keyword start="(" matchgroup=Keyword end=")" contains=ALL

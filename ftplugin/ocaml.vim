set expandtab copyindent preserveindent
set softtabstop=0 shiftwidth=2 tabstop=2

map <Leader>t :MerlinTypeOf<Enter>
map <Leader>b :MerlinLocate<Enter>
map <Leader>m :make compile<Enter>

let g:SuperTabDefaultCompletionType = "<c-x><c-o>"

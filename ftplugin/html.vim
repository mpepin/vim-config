set expandtab copyindent preserveindent
set softtabstop=0 shiftwidth=2 tabstop=2
set textwidth=0

nnoremap <Leader>f :setfiletype htmldjango<Enter>

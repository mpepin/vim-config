set expandtab copyindent preserveindent
set softtabstop=0 shiftwidth=2 tabstop=2

let g:SuperTabDefaultCompletionType = "<c-x><c-o>"
